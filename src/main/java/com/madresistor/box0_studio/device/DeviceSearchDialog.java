/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0_studio.device;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.app.AlertDialog;

import com.madresistor.box0_studio.R;
import com.madresistor.box0_studio.device.DeviceManager;

public class DeviceSearchDialog extends AlertDialog
		implements DialogInterface.OnClickListener {
	private final DeviceManager m_deviceManager;
	public DeviceSearchDialog(DeviceManager deviceManager,
			Context context, int title_res_id) {
		super(context);

		m_deviceManager = deviceManager;
		setIcon(R.drawable.ic_usb_grey600_48dp);

		Resources res = context.getResources();
		setTitle(res.getString(title_res_id));
		setMessage(res.getString(R.string.try_again));
		setButton(DialogInterface.BUTTON_POSITIVE,
			res.getString(R.string.retry), this);
	}

	public void onClick(DialogInterface dialog, int which) {
		/* again search for device in an new thread */
		new Handler().post(new Runnable() {
			public void run() {
				m_deviceManager.search();
			}
		});
	}
}

/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0_studio.nav_drawer;

import com.madresistor.box0_studio.R;

public class NavDrawerModule extends AbsNavDrawerItem {
	public NavDrawerModule(String title, int icon) {
		super(R.layout.nav_drawer_module,
			R.id.nav_drawer_module_title, title,
			R.id.nav_drawer_module_icon, icon);
	}
}

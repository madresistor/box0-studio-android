/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0_studio.extra.input_filter;

/**
 * Check if the provided value is a valid frequency
 * In __real world__, only frequencies are possible.
 * And we are assuming we are in real world
 */
public class FreqInputFilter extends AbsFloatingInputFilter
{
	/**
	 * @return true if greater than 0
	 */
	protected boolean isValid(double val) {
		return (val > 0f);
	}
}

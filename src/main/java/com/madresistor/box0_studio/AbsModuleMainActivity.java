/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0_studio;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.madresistor.box0.Device;
import com.madresistor.box0_studio.intrument.Intrument;
import com.madresistor.box0_studio.intrument.func_gen.FuncGen;
import com.madresistor.box0_studio.intrument.dio_panel.DioPanel;
import com.madresistor.box0_studio.intrument.osc.Osc;
import com.madresistor.box0_studio.intrument.pwm_panel.PwmPanel;

abstract class AbsModuleMainActivity extends AbsDeviceMainActivity {
	/* used for Log */
	private final static String TAG = AbsModuleMainActivity.class.getName();

	/* modules index */
	public final static int OSC = 0;
	public final static int FUNC_GEN = 1;
	public final static int PWM_PANEL = 2;
	public final static int DIO_PANEL = 3;

	/* modules */
	private Intrument[] m_modules;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		m_modules = new Intrument[] {
			new Osc(this),
			new FuncGen(this),
			new PwmPanel(this),
			new DioPanel(this)
		};

		super.onCreate(savedInstanceState);
	}

	//~ @Override
	//~ public void onConfigurationChanged(Configuration newConfig) {
		//~ super.onConfigurationChanged(newConfig);
		//~
		//~ if (Config.DEBUG) Log.d(TAG, "configuration changed");
//~
		//~ /* force module to update their views */
		//~ for (Module module : m_modules) {
			//~ module.getFrontend().onConfigurationChanged(newConfig);
		//~ }
	//~ }

	@Override
	protected void onDestroy() {
		super.onDestroy();

		for (Intrument m : m_modules) {
			m.freeResource();
		}
	}

	@Override
	protected void deviceChanged(Device box0Device) {
		/* notify each module about device */
		for (Intrument m : m_modules) {
			m.setDevice(box0Device);
		}

		/* update the option menu */
		supportInvalidateOptionsMenu();
	}

	protected Fragment getModuleFragment(int index) {
		if (index < m_modules.length){
			return m_modules[index].getFrontend();
		}

		throw new IllegalArgumentException("index is not a valid module number");
	}
}

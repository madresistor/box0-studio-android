/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0_studio.intrument.dio_panel;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.Dio;
import com.madresistor.box0_studio.Config;
import com.madresistor.box0_studio.R;
import com.madresistor.box0_studio.intrument.Intrument;
import com.madresistor.box0_studio.intrument.dio_panel.Adapter;

public class DioPanel extends Intrument {
	private final static String TAG = Dio.class.getName();
	private boolean m_running;
	private Dio m_box0Dio;
	private OnStatusListener m_onStatusListener;
	private final Adapter m_adapter;

	private Frontend m_frontend = new Frontend();

	public DioPanel(Context context) {
		super(context);
		m_adapter = new Adapter(context);
	}

	public void setOnStatusListener(Intrument.OnStatusListener onStatusListener) {
		m_onStatusListener = onStatusListener;
	}

	@Override
	public Frontend getFrontend() {
		return m_frontend;
	}

	public void setDevice(Device box0Device) {
		if (m_box0Dio != null) {
			try {
				/* tell the adapter that module is no more.
				 *  A thread is running to read input pins. */
				m_adapter.setModule(null);
				m_box0Dio.basicStop();
				m_box0Dio.close();
			} catch (ResultException e) {
				//sheh, just ignoring this
			}
		}

		m_box0Dio = null;
		if (box0Device == null) {
			/* nothing more */
			return;
		}

		try {
			/* get the first DIO module */
			m_box0Dio = box0Device.dio(0);
			m_box0Dio.basicPrepare();
			m_box0Dio.basicStart();

			/* TODO: show channel name */

		} catch(ResultException e) {
			if (Config.WARN) Log.w(TAG, "error while initalizing DIO " + e + ": " + e.explain());
			Activity activity = m_frontend.getActivity();
			if (activity != null) {
				int msg = R.string.dio_init_failed;
				Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
			}

			m_box0Dio = null;
		}

		m_adapter.setModule(m_box0Dio);
		m_frontend.sync();
	}

	public void freeResource() {
		if (m_box0Dio != null) {
			try {
				m_box0Dio.close();
			} catch (ResultException e) {
				//sheh, just ignoring this
			}
		}

		m_box0Dio = null;
		m_adapter.setModule(m_box0Dio);
		m_frontend.sync();
	}

	private class Frontend extends Intrument.Frontend {
		public Frontend() {
			/* Fragment participate in menu creation */
			setHasOptionsMenu(true);

			/* Retain state */
			setRetainInstance(true);
		}

		private MenuItem m_actionHizAll;
		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			inflater.inflate(R.menu.dio_actions, menu);

			m_actionHizAll = menu.findItem(R.id.dio_action_hiz_all);
		}

		@Override
		public void onDestroyOptionsMenu () {
			m_actionHizAll = null;
			super.onDestroyOptionsMenu();
		}

		/***
		 * Called when invalidateOptionsMenu() is triggered
		 */
		@Override
		public void onPrepareOptionsMenu(Menu menu) {
			super.onPrepareOptionsMenu(menu);
			m_actionHizAll.setEnabled(m_box0Dio != null);
			m_actionHizAll.setChecked(m_adapter.isAllHiz());
		}

		@Override
		public boolean onOptionsItemSelected (MenuItem item) {
			switch(item.getItemId()) {
			case R.id.dio_action_hiz_all:
				boolean hiz = !m_actionHizAll.isChecked();
				m_adapter.hizAll(hiz);
				m_actionHizAll.setChecked(m_adapter.isAllHiz());
			return true;
			default:
			return super.onOptionsItemSelected(item);
			}
		}

		private ListView m_pins;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_dio_panel, container, false);

			m_pins = (ListView) rootView.findViewById(
				R.id.fragment_dio_panel_pins);
			m_pins.setAdapter(m_adapter);
			m_pins.setOnItemClickListener(m_adapter);
			m_pins.setOnItemLongClickListener(m_adapter);

			return rootView;
		}

		@Override
		public void onDestroyView() {
			m_pins = null;

			super.onDestroyView();
		}

		public void sync() {
			if (m_pins != null) {
				m_pins.setEnabled(m_box0Dio != null);
			}

			if (m_actionHizAll != null) {
				m_actionHizAll.setEnabled(m_box0Dio != null);
			}
		}
	}
}

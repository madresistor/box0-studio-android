/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0_studio.intrument.osc;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.os.Handler;

import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.Ain;
import com.madresistor.box0_studio.Config;
import com.madresistor.box0_studio.R;
import com.madresistor.box0_studio.extra.ProblemDialog;
import com.madresistor.box0_studio.extra.SettingsFragment;
import com.madresistor.box0_studio.intrument.Intrument;
import com.madresistor.box0_studio.intrument.osc.Plot;
import com.madresistor.box0_studio.ui.MultiStringSpinner;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/*
 * TODO: Lissajous plot
 */

public class Osc extends Intrument {
	private final static String TAG = Osc.class.getName();

	private boolean m_running = false;
	private Ain m_box0Ain = null;

	private List<Long> m_values_speed = null;
	private List<String> m_values_speed_str = null;
	private List<Integer> m_values_bitsize = null;
	private List<String> m_values_bitsize_str = null;
	private List<String> m_values_channel_str = null;
	private List<Integer> m_values_channel_color = new ArrayList<Integer>();

	private Intrument.OnStatusListener m_onStatusListener = null;

	private Frontend m_frontend = new Frontend();

	public Osc(Context context) {
		super(context);
	}

	@Override
	public void setOnStatusListener(Intrument.OnStatusListener onStatusListener) {
		m_onStatusListener = onStatusListener;
	}

	@Override
	public Frontend getFrontend() {
		return m_frontend;
	}

	/**
	 * Build channel list from AIN module
	 */
	private void buildChannelList() {
		if (m_box0Ain == null) {
			if (Config.DEBUG) Log.d(TAG, "m_box0Ain is null, cannot build list");
			return;
		}

		Resources resources = getContext().getResources();
		String format_no_name_ch = resources.getString(R.string.format_no_name_ch);

		List<String> list = new ArrayList<String>();

		for (int i = 0; i < m_box0Ain.chanCount; i++) {
			String title = title = m_box0Ain.label.chan[i];

			if (title == null) {
				title = String.format(format_no_name_ch, i);
			}

			list.add(title);
		}

		m_values_channel_str = list;
	}

	/**
	 * Build channel color
	 */
	private void buildChannelColor() {
		if (m_values_channel_str == null) {
			if (Config.DEBUG) Log.d(TAG, "Cannot build channel color because m_values_channel_str is null");
			return;
		}

		int FILLED = m_values_channel_color.size();
		int COUNT = m_values_channel_str.size();
		for (int i = FILLED; i < COUNT; i++) {
			int color = Intrument.newColorForUser();
			m_values_channel_color.add(color);
		}
	}

	/**
	 * Build bitsize list from AIN module
	 */
	private void buildBitsizeList() {
		if (m_box0Ain == null) {
			if (Config.DEBUG) Log.d(TAG, "m_box0Ain is null, cannot build list");
			return;
		}

		Resources resources = getContext().getResources();
		String format_bitsize = resources.getString(R.string.format_bitsize);

		List<Integer> values = new ArrayList<Integer>();
		List<String> strings = new ArrayList<String>();

		for (Ain.BitsizeSpeeds bss: m_box0Ain.stream) {
			int v = bss.bitsize;
			if (!values.contains(v)) {
				values.add(v);
				strings.add(String.format(format_bitsize, v));
			}
		}

		m_values_bitsize = values;
		m_values_bitsize_str = strings;
	}

	/**
	 * Invalidate the current speed list.
	 * The list will be rebuild based on the current bitsize
	 */
	private void invalidateSpeedList() {
		if (m_box0Ain == null) {
			if (Config.DEBUG) Log.d(TAG, "m_box0Ain is null");
			return;
		}

		int bitsize = m_frontend.getBitsize();
		if (bitsize <= 0) {
			if (Config.WARN) Log.w(TAG, "getBitsize() returned invalid bitsize");
			return;
		}

		/* read all speed and add them to list */
		m_values_speed = new ArrayList<Long>();
		m_values_speed_str = new ArrayList<String>();

		Resources resources = getContext().getResources();
		String format_S_s = resources.getString(R.string.format_S_s);
		String format_KS_s = resources.getString(R.string.format_KS_s);

		for (Ain.BitsizeSpeeds bss: m_box0Ain.stream) {
			if (bss.bitsize != bitsize) {
				continue;
			}

			for (int i = 0; i < bss.speed.length; i++) {
				long speed = bss.speed.get(i);

				String value_str;
				if (speed >= 1000) {
					value_str = String.format(format_KS_s, speed / 1000);
				} else {
					value_str = String.format(format_S_s, speed);
				}

				m_values_speed.add(speed);
				m_values_speed_str.add(value_str);
			}

			break;
		}
	}

	/**
	 * Close the current AIN module
	 */
	private void closeAin() {
		if (m_box0Ain == null) {
			if (Config.DEBUG) Log.d(TAG, "no module to close");
			return;
		}

		// debug why this hangs and never return
		//if (m_running) {
		//	stopAin();
		//}

		try {
			m_box0Ain.close();
		} catch (ResultException e) {
			if (Config.WARN) Log.w(TAG, "error while closing AIN " + e + ": " + e.getMessage());
		}

		m_box0Ain = null;
	}

	public void setDevice(Device box0Device) {
		closeAin();

		if (box0Device == null) {
			return;
		}

		try {
			/* get the first AIN module */
			m_box0Ain = box0Device.ain(0);
		} catch(ResultException e) {
			if (Config.WARN) Log.w(TAG, "error while initalizing AIN " + e + ": " + e.getMessage());

			Activity activity = m_frontend.getActivity();
			if (activity != null) {
				int msg = R.string.ain_init_failed;
				Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
			}

			m_box0Ain = null;
		}

		buildChannelList();
		buildChannelColor();
		m_frontend.syncChannel();

		buildBitsizeList();
		m_frontend.syncBitsize();

		invalidateSpeedList();
		m_frontend.syncSpeed();

		setRunning(false);
	}

	/**
	 * Start AIN module
	 */
	private void startAin() {
		if (m_box0Ain == null) {
			if (Config.WARN) Log.w(TAG, "m_box0Ain is null");
			return;
		}

		Activity activity = m_frontend.getActivity();
		if (activity == null) {
			if (Config.DEBUG) Log.d(TAG, "getActivity() returned null");
			return;
		}

		int[] channelSeq = m_frontend.getChannelSequence();
		if (Config.DEBUG) Log.d(TAG, "channel sequence: " + channelSeq);
		if (channelSeq == null) {
			new ProblemDialog(activity,
				R.string.channl_sequence_empty,
				R.string.channl_sequence_empty_desc);
			return;
		}

		int bitsize = m_frontend.getBitsize();
		if (Config.DEBUG) Log.d(TAG, "bitsize: " + bitsize);
		if (bitsize <= 0) {
			new ProblemDialog(activity,
				R.string.bitsize_not_selected,
				R.string.bitsize_not_selected_desc);
			return;
		}

		long speed = m_frontend.getSpeed();
		if (Config.DEBUG) Log.d(TAG, "speed: " + speed);
		if (speed <= 0) {
			new ProblemDialog(activity,
				R.string.speed_not_selected,
				R.string.speed_not_selected_desc);
			return;
		}

		/* Read "flow plot style" setting */
		SharedPreferences sharedPref =
			PreferenceManager.getDefaultSharedPreferences(activity);

		boolean flowPlot = sharedPref.getBoolean(
			SettingsFragment.PREF_OSC_PLOT_STYLE,
			SettingsFragment.DEF_OSC_PLOT_STYLE);

		/* set line width */
		int lineWidth = sharedPref.getInt(
			SettingsFragment.PREF_LINE_WIDTH,
			SettingsFragment.DEF_LINE_WIDTH);

		/* build color list */
		int COUNT = channelSeq.length;
		final int[] colors = new int[COUNT];
		for (int i = 0; i < COUNT; i++) {
			int j = channelSeq[i];
			colors[i] = m_values_channel_color.get(j);
		}

		/* Try to start the process */
		try {
			box0AinStreamStart(m_box0Ain.getPointer(),
				bitsize, speed, channelSeq, flowPlot, colors, lineWidth);
		} catch(ResultException e) {
			Log.d(TAG, "unable to start AIN " + e + ":" + e.getMessage());
			new ProblemDialog(activity,
				R.string.osc_start_fail,
				R.string.osc_start_fail_desc);
			return;
		}

		setRunning(true);
	}

	/**
	 * Stop AIN module.
	 */
	private void stopAin() {
		if (m_box0Ain == null) {
			if (Config.WARN) Log.w(TAG, "m_box0Ain is null");
		}

		try {
			box0AinStreamStop(m_box0Ain.getPointer());
		} catch(ResultException e) {
			if (Config.WARN) Log.w(TAG, "box0 exception " + e);
		}

		setRunning(false);
	}

	/**
	 * Mark the AIN module as Running
	 */
	private void setRunning(boolean running) {
		m_running = running;
		m_frontend.syncStartStop();
	}

	public void freeResource() {
		closeAin();
		setRunning(false);
	}

	private final Handler m_handler = new Handler();

	private class Frontend extends Intrument.Frontend
						implements AdapterView.OnItemSelectedListener {
		private final String TAG = Frontend.class.getName();

		public Frontend() {
			/* Fragment participate in menu creation */
			setHasOptionsMenu(true);

			/* Retain state */
			setRetainInstance(true);
		}

		private MultiStringSpinner m_channel = null;
		private Spinner m_speed = null;
		private Spinner m_bitsize = null;
		private Plot m_plot = null;
		private MenuItem m_actionStartStop = null;

		/**
		 * Synchronize m_bitsize and bitsize list
		 */
		public void syncBitsize() {
			if (m_bitsize == null) {
				if (Config.WARN) Log.w(TAG, "m_bitsize is null");
				return;
			}

			if (m_values_bitsize_str == null) {
				if (Config.WARN) Log.w(TAG, "m_values_bitsize_str is null");
				return;
			}

			Activity activity = getActivity();
			if (activity == null) {
				if (Config.WARN) Log.w(TAG, "Fragment not bind to activity, getActivity()  returned null");
				return;
			}

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				activity, android.R.layout.simple_spinner_item,
				m_values_bitsize_str);
			adapter.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item);
			m_bitsize.setAdapter(adapter);
		}

		/**
		 * Synchronize m_channel and channel list
		 */
		public void syncChannel() {
			if (m_channel == null) {
				if (Config.WARN) Log.w(TAG, "m_channel is null");
				return;
			}

			if (m_values_channel_str == null) {
				if (Config.WARN) Log.w(TAG, "m_values_channel_str_str is null");
				return;
			}

			List<Integer> selected = m_channel.getSelectedIndicies();
			m_channel.setItems(m_values_channel_str);
			if (selected.isEmpty()) {
				Integer[] ch = {0};
				m_channel.setSelection(ch);
			} else {
				m_channel.setSelection(selected);
			}

			m_channel.setColors(m_values_channel_color);
		}

		/**
		 * Synchronize m_running and m_actionStartStop menu item
		 */
		public void syncStartStop() {
			if (m_actionStartStop == null) {
				if (Config.WARN) Log.w(TAG, "m_actionStartStop is null");
				return;
			}

			int string;
			int icon;

			if (m_running) {
				icon = R.drawable.ic_stop_grey600_48dp;
				string = R.string.stop;
			} else {
				icon = R.drawable.ic_play_grey600_48dp;
				string = R.string.start;
			}

			m_actionStartStop.setTitle(string);
			m_actionStartStop.setIcon(icon);
		}

		/**
		 * Synchronize speed with current speed
		 */
		public void syncSpeed() {
			if (m_speed == null) {
				if (Config.WARN) Log.w(TAG, "m_speed is null");
				return;
			}

			if (m_values_speed_str == null) {
				if (Config.WARN) Log.w(TAG, "m_values_speed_str is null");
				return;
			}

			Activity activity = getActivity();
			if (activity == null) {
				if (Config.WARN) Log.w(TAG, "Fragment not bind to activity, getActivity()  returned null");
				return;
			}

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				activity, android.R.layout.simple_spinner_item,
				m_values_speed_str);
			adapter.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item);
			int last_index = m_speed.getSelectedItemPosition();
			m_speed.setAdapter(adapter);
			m_speed.setSelection(last_index);
		}

		/**
		 * Get the selected speed.
		 * <p>return zero or negative value if problem occured.</p>
		 * @return selected speed
		 */
		public long getSpeed() {
			if (m_speed == null) {
				if (Config.DEBUG) Log.d(TAG, "m_speed is null");
				return -1;
			}

			int index = m_speed.getSelectedItemPosition();
			if (index >= 0 && index < m_values_speed.size()) {
				return m_values_speed.get(index);
			}

			return -1;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			if (Config.DEBUG) Log.d(TAG, "inside onViewCreated");
			return inflater.inflate(R.layout.fragment_osc, container, false);
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			if (Config.DEBUG) Log.d(TAG, "inside onViewCreated");
			super.onViewCreated(view, savedInstanceState);

			m_channel = (MultiStringSpinner) view.findViewById(R.id.osc_channel);
			m_speed = (Spinner) view.findViewById(R.id.osc_speed);
			m_bitsize = (Spinner) view.findViewById(R.id.osc_bitsize);
			m_plot = (Plot) view.findViewById(R.id.osc_plot);

			syncBitsize();
			//syncSpeed();
			syncChannel();
		}

		@Override
		public void onResume() {
			if (Config.DEBUG) Log.d(TAG, "inside onResume");
			super.onResume();

			/* why post on a runnable instead of directly doing the stuff in onResume.
			 * Because: Unwanted callback from
			 *  - spinner [item selected]
			 * This prevent unwanted callback for the first time.
			 */
			m_handler.post(new Runnable() {
				@Override
				public void run() {
					invalidateSpeedList();
					syncSpeed();
					/* Note: ^^^^ should have been called
					 *  from onViewStateRestored() but did not work.
					 *  Inputs where not enabled for the crossponding waveform.
					 *  probebly the spinner was not giving correct selected index
					 */

					m_bitsize.setOnItemSelectedListener(Frontend.this);
					m_channel.setOnItemSelectedListener(Frontend.this);
					m_speed.setOnItemSelectedListener(Frontend.this);
				}
			});
		}

		@Override
		public void onDestroyView() {
			if (Config.DEBUG) Log.d(TAG, "inside onDestroyView");
			super.onDestroyView();

			m_channel = null;
			m_speed = null;
			m_bitsize = null;
			m_plot = null;
		}

		/**
		 * Get the currently selected bitsize
		 * <p>If return zero 0 or negative value, then there is some error</p>
		 * @return bitsize
		 */
		public int getBitsize() {
			if (m_bitsize == null) {
				if (Config.WARN) Log.d(TAG, "m_bitsize is null");
				return -1;
			}

			if (m_values_bitsize == null) {
				if (Config.WARN) Log.d(TAG, "m_values_bitsize is null");
				return -1;
			}

			int index = m_bitsize.getSelectedItemPosition();
			if (index >= 0 && index < m_values_bitsize.size()) {
				return m_values_bitsize.get(index);
			}

			return -1;
		}

		/**
		 * Build and return the selected channels
		 * <p>return null on problem</p>
		 * @return selected channels
		 */
		public int[] getChannelSequence() {
			if (m_channel == null) {
				if (Config.DEBUG) Log.d(TAG, "m_channel is null");
				return null;
			}

			List<Integer> channels = m_channel.getSelectedIndicies();
			if (channels == null) {
				if (Config.DEBUG) Log.d(TAG, "m_channel returned null");
				return null;
			}

			if (channels.isEmpty()) {
				if (Config.DEBUG) Log.d(TAG, "m_channel returned empty list");
				return null;
			}

			int COUNT = channels.size();
			int[] chanSeq = new int[COUNT];
			for (int i = 0; i < COUNT; i++) {
				chanSeq[i] = channels.get(i).intValue();
			}

			return chanSeq;
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			if (Config.DEBUG) Log.d(TAG, "inside onCreateOptionsMenu");
			super.onCreateOptionsMenu(menu, inflater);

			inflater.inflate(R.menu.osc_actions, menu);
			m_actionStartStop = menu.findItem(R.id.osc_action_start_stop);
		}

		/***
		 * Called when invalidateOptionsMenu() is triggered
		 */
		@Override
		public void onPrepareOptionsMenu(Menu menu) {
			if (Config.DEBUG) Log.d(TAG, "inside onPrepareOptionsMenu");
			super.onPrepareOptionsMenu(menu);

			m_actionStartStop.setVisible(m_box0Ain != null);
			syncStartStop();
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			if (Config.DEBUG) Log.d(TAG, "inside onOptionsItemSelected");

			switch(item.getItemId()) {
			case R.id.osc_action_start_stop:
				if (m_running) {
					stopAin();
				} else {
					startAin();
				}
			return true;
			default:
			return super.onOptionsItemSelected(item);
			}
		}

		@Override
		public void onDestroyOptionsMenu() {
			if (Config.DEBUG) Log.d(TAG, "inside onDestroyOptionsMenu");
			super.onDestroyOptionsMenu();

			m_actionStartStop = null;
		}

		/**
		 * Callback when user select a different configuration.
		 *  - bitsize
		 *  - speed
		 *  - channels
		 */
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,  int pos, long id) {
			if (m_bitsize == parent) {
				if (m_values_bitsize == null) {
					if (Config.DEBUG) Log.w(TAG, "m_values_bitsize is null");
					return;
				}

				/* rebuild the speed list */
				invalidateSpeedList();
				syncSpeed();
			}

			/* restart if we are running */
			if (m_running) {
				stopAin();
				startAin();
			}
		}

		public void onNothingSelected(AdapterView<?> parent) {}
	}

	/**
	 * Place a request render on the plot.
	 * <p>C code call this function when data is changed </p>
	 */
	public void replot() {
		if (m_frontend.m_plot == null) {
			if (Config.WARN) Log.w(TAG, "cannot request render as m_plot is null");
			return;
		}

		m_frontend.m_plot.requestRender();
	}

	/**
	 * Start AIN module in stream mode.
	 * <p>Due to effeciency reason, the whole
	 *  start, copying and update is done in C</p>
	 * <p>Copying of data directly to OpenGLES buffer is done</p>
	 * @param mod AIN module (b0_ain*)
	 * @param bitsize Stream bitsize
	 * @param speed Stream speed
	 * @param chan_seq Channel Sequence
	 * @param flowPlot Use Flow Plot if true
	 * @param colors Colors for the channels
	 * @param line_width Line width of the channels
	 * @throws ResultException on error result code
	 */
	private native void box0AinStreamStart(
		ByteBuffer /* (b0_ain*) */ mod,
		int bitsize, long speed,
		int[] chan_seq, boolean flowPlot,
		int[] colors, float line_width) throws ResultException;

	/**
	 * Stop the AIN module
	 * @param mod AIN module (b0_ain*)
	 * @throws ResultException on error result code
	 */
	private native static void box0AinStreamStop(
		ByteBuffer /* (b0_ain*) */ mod) throws ResultException;
}

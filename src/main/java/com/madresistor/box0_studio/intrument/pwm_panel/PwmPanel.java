/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0_studio.intrument.pwm_panel;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.madresistor.box0.Device;
import com.madresistor.box0.ResultException;
import com.madresistor.box0.module.Pwm;
import com.madresistor.box0_studio.Config;
import com.madresistor.box0_studio.R;
import com.madresistor.box0_studio.extra.ProblemDialog;
import com.madresistor.box0_studio.extra.input_filter.AbsFloatingInputFilter;
import com.madresistor.box0_studio.extra.input_filter.FreqInputFilter;
import com.madresistor.box0_studio.extra.input_filter.PercentInputFilter;
import com.madresistor.box0_studio.intrument.Intrument;

public class PwmPanel extends Intrument {
	private final static String TAG = Pwm.class.getName();
	private boolean m_running;
	private Pwm m_box0Pwm;
	private OnStatusListener m_onStatusListener;

	private Frontend m_frontend = new Frontend();

	public PwmPanel(Context context) {
		super(context);
	}

	public void setOnStatusListener(Intrument.OnStatusListener onStatusListener) {
		m_onStatusListener = onStatusListener;
	}

	@Override
	public Frontend getFrontend() {
		return m_frontend;
	}

	public void setDevice(Device box0Device) {
		if (m_box0Pwm != null) {
			try {
				m_box0Pwm.close();
			} catch (ResultException e) {
				//sheh, just ignoring this
			}
		}

		m_box0Pwm = null;
		if (box0Device == null) {
			/* nothing more */
			return;
		}

		try {
			/* get the first PWM module */
			m_box0Pwm = box0Device.pwm(0);
			m_box0Pwm.outputPrepare();

			/* TODO: show channel name */

		} catch(ResultException e) {
			if (Config.WARN) Log.w(TAG, "error while initalizing PWM " + e + ": " + e.explain());
			Activity activity = m_frontend.getActivity();
			if (activity != null) {
				int msg = R.string.pwm_init_failed;
				Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
			}

			m_box0Pwm = null;
		}

		setRunning(false);
	}

	public void freeResource() {
		if (m_box0Pwm != null) {
			try {
				m_box0Pwm.close();
			} catch (ResultException e) {
				//sheh, just ignoring this
			}
		}

		m_box0Pwm = null;
		setRunning(false);
	}

	public void startPwm() {
		if (m_box0Pwm == null) {
			if (Config.WTF) Log.wtf(TAG, "box0Pwm is null");
			/* TODO: show error to user */
			return;
		}

		EquationParam param = m_frontend.prepareEquationParam();

		new StartPwm().execute(param);
	}

	private static class DutyCycleInputFilter extends AbsFloatingInputFilter {
		protected boolean isValid(double value) {
			return (value > 0f && value < 100f);
		}
	}

	private class StartPwm extends AsyncTask<EquationParam, Void, EquationResult> {
		private final String TAG = "StartPwm";
		protected EquationResult doInBackground(EquationParam... params) {
			EquationResult result = new EquationResult();
			result.error_title = result.error_desc = null;
			Resources resources = m_frontend.getActivity().getResources();
			EquationParam p0 = params[0];
			Pwm.OutputCalc res;

			try {
				res = m_box0Pwm.outputCalc(p0.bitsize, p0.freq, p0.error);
			} catch(ResultException e) {
				/* store the error */
				result.error_desc = "Unable to find any configuration";
				result.error_title = "Calculation Failed";
				return result;
			}

			/* configure, send data to device and start */
			try {
				m_box0Pwm.setSpeed(res.speed);
				m_box0Pwm.setPeriod(res.period);
				m_box0Pwm.setWidth(p0.pin, res.calcWidth(p0.duty_cycle));
				m_box0Pwm.outputStart();
			} catch(ResultException e) {
				result.error_title = resources.getString(R.string.pwm_start_failed);
				result.error_desc = resources.getString(R.string.pwm_start_failed_desc);
				return result;
			}

			return result;
		}

		protected synchronized void onPostExecute(EquationResult result) {
			boolean errorOccured = (result.error_title != null || result.error_desc != null);
			setRunning(!errorOccured);
			if (errorOccured) {
				new ProblemDialog(
					m_frontend.getActivity(),
					result.error_title,
					result.error_desc);
			}
		}
	}

	public void stopPwm() {
		if (m_box0Pwm == null) {
			if (Config.WTF) Log.wtf(TAG, "box0Pwm is null");
			/* TODO: show error to user */
			return;
		}

		try {
			m_box0Pwm.outputStop();
		} catch(ResultException e) {
			if (Config.WARN) Log.w(TAG, "error while stopping PWM" + e.explain() + ": " + e);
		}

		setRunning(false);
	}

	private void setRunning(boolean running) {
		/* mark as not running */
		m_running = running;
		m_frontend.syncStartStop();
		m_frontend.syncUiInteractibility();
	}

	private class EquationParam {
		public int pin;
		public int bitsize;
		public double freq;
		public double duty_cycle;
		public double error;
	};

	public class EquationResult {
		public double freq;
		public double error;

		/* description if error occured */
		public String error_title, error_desc;
	}

	private class Frontend extends Intrument.Frontend {
		private EditText m_freq = null;
		private EditText m_dutyCycle = null;
		private EditText m_error = null;

		private MenuItem m_actionStartStop = null;
		private MenuItem m_actionCalcMinError = null;

		public Frontend() {
			/* Fragment participate in menu creation */
			setHasOptionsMenu(true);

			/* Retain state */
			setRetainInstance(true);
		}

		public void syncStartStop() {
			if (m_actionStartStop == null) {
				if (Config.WARN) Log.w(TAG, "m_actionStartStop is null");
				return;
			}

			int string;
			int icon;

			if (m_running) {
				icon = R.drawable.ic_stop_grey600_48dp;
				string = R.string.stop;
			} else {
				icon = R.drawable.ic_play_grey600_48dp;
				string = R.string.start;
			}

			m_actionStartStop.setTitle(string);
			m_actionStartStop.setIcon(icon);
		}

		public void syncUiInteractibility() {
			boolean inputInteract = !m_running;
			if (m_freq != null) m_freq.setEnabled(inputInteract);
			if (m_dutyCycle != null) m_dutyCycle.setEnabled(inputInteract);
			if (m_error != null) m_error.setEnabled(inputInteract);
		}

		public EquationParam prepareEquationParam() {
			if (m_box0Pwm == null) {
				return null;
			}

			EquationParam param = new EquationParam();

			param.bitsize = m_box0Pwm.bitsize.get(0); /* First bitsize */
			param.pin = 0;

			/* freq */
			String freqText = m_freq.getText().toString();
			if (freqText.length() < 1) {
				if (Config.DEBUG) Log.d(TAG, "freq string empty");
				return null;
			}
			param.freq = Double.parseDouble(freqText);

			/* duty cycle */
			String dutyCycleText = m_dutyCycle.getText().toString();
			if (dutyCycleText.length() < 1) {
				if (Config.DEBUG) Log.d(TAG, "duty cycle string empty");
				return null;
			}
			param.duty_cycle = Double.parseDouble(dutyCycleText);

			/* error
			 * UI Note: if user entered nothing, assume 0
			 */
			String errorText = m_error.getText().toString();
			param.error = (errorText.length() < 1) ? 0 : Double.parseDouble(errorText);

			if (Config.DEBUG) {
				Log.d(TAG, "freq: " + param.freq);
				Log.d(TAG, "duty_cycle: " + param.duty_cycle);
				Log.d(TAG, "error: " + param.error);
			}

			return param;
		}

		private class CalcMinError extends AsyncTask<EquationParam, Void, EquationResult> {
			protected EquationResult doInBackground(EquationParam... params) {
				EquationResult result = new EquationResult();
				try {
					EquationParam p0 = params[0];
					Pwm.OutputCalc res = m_box0Pwm.outputCalc(p0.bitsize, p0.freq, p0.error);
					result.freq = m_box0Pwm.outputCalcFreq(res.speed, res.period);
					result.error = m_box0Pwm.outputCalcFreqError(result.freq, p0.freq);
				} catch(ResultException e) {
					/* store the error */
					result.error_desc = e.explain();
					result.error_title = "Calculation error";
				}

				return result;
			}

			protected synchronized void onPostExecute(EquationResult result) {
				boolean errorOccured = (result.error_title != null) || (result.error_desc != null);
				if (errorOccured) {
					/* show the error */
					new ProblemDialog(
						getActivity(),
						result.error_title,
						result.error_desc);
					return;
				}

				m_error.setText(Double.toString(result.error));
			}
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			inflater.inflate(R.menu.pwm_actions, menu);
			m_actionStartStop = menu.findItem(R.id.pwm_action_start_stop);
			m_actionCalcMinError = menu.findItem(R.id.pwm_action_calc_min_error);

			boolean hasModule = m_box0Pwm != null;
			m_actionStartStop.setVisible(hasModule);
			m_actionCalcMinError.setVisible(hasModule);

			/* update the start stop action */
			syncStartStop();
			syncUiInteractibility();
		}

		@Override
		public void onDestroyOptionsMenu () {
			m_actionStartStop = null;
			m_actionCalcMinError = null;
			super.onDestroyOptionsMenu();
		}

		/***
		 * Called when invalidateOptionsMenu() is triggered
		 */
		@Override
		public void onPrepareOptionsMenu(Menu menu) {
			boolean hasModule = m_box0Pwm != null;
			m_actionStartStop.setVisible(hasModule);
			m_actionCalcMinError.setVisible(hasModule);
			super.onPrepareOptionsMenu(menu);
		}

		@Override
		public boolean onOptionsItemSelected (MenuItem item) {
			switch(item.getItemId()) {
			case R.id.pwm_action_start_stop:
				if (m_running) {
					stopPwm();
				} else {
					startPwm();
				}
			return true;
			case R.id.pwm_action_calc_min_error:
				calcMinError();
			return true;
			default:
			return super.onOptionsItemSelected(item);
			}
		}

		void calcMinError() {
			EquationParam param = prepareEquationParam();

			if (param != null) {
				new CalcMinError().execute(param);
			}
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_pwm, container, false);

			if (Config.DEBUG) Log.d(TAG, "inside onCreateView");

			m_freq = (EditText) rootView.findViewById(R.id.pwm_freq);
			m_dutyCycle = (EditText) rootView.findViewById(R.id.pwm_duty_cycle);
			m_error = (EditText) rootView.findViewById(R.id.pwm_error);

			m_freq.setFilters(new InputFilter [] { new FreqInputFilter() });
			m_dutyCycle.setFilters(new InputFilter [] { new DutyCycleInputFilter() });
			m_error.setFilters(new InputFilter [] { new PercentInputFilter() });

			syncUiInteractibility();

			return rootView;
		}

		@Override
		public void onDestroyView() {
			m_freq = null;
			m_dutyCycle = null;
			m_error = null;

			super.onDestroyView();
		}
	}
}

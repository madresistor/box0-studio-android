/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright 2013 Gunaseelan A <gunasiet@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.madresistor.box0_studio.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.database.DataSetObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.WrapperListAdapter;

import com.madresistor.box0_studio.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MultiStringSpinner extends Spinner implements OnMultiChoiceClickListener {
	private final static String TAG = MultiStringSpinner.class.getName();

	String[] m_items = null;
	boolean[] m_selectedItems = null;
	private final static String STATE_ITEMS = "items";
	private final static String STATE_SELECTED_ITEMS = "selectedItems";
	int[] m_items_color = null;

	ArrayAdapter<String> m_simpleStringAdapter;

	public MultiStringSpinner(Context context) {
		super(context);
		init(context);
	}

	public MultiStringSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		m_simpleStringAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
		super.setAdapter(m_simpleStringAdapter);
	}

	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
		if (m_selectedItems != null && which < m_selectedItems.length) {
			m_selectedItems[which] = isChecked;

			m_simpleStringAdapter.clear();
			m_simpleStringAdapter.add(concatSelectedStrings());
		} else {
			throw new IllegalArgumentException("Argument 'which' is out of bounds.");
		}
	}

	@Override
	public boolean performClick() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
		builder.setMultiChoiceItems(m_items, m_selectedItems, this);
		AlertDialog dialog = builder.show();

		ListView list = dialog.getListView();
		ListAdapter listAdapter = list.getAdapter();
		listAdapter = new StylerWrapperListAdapter(listAdapter);
		list.setAdapter(listAdapter);

		//~ dialog.show();
		return true;
	}

	private class StylerWrapperListAdapter implements WrapperListAdapter {
		private final ListAdapter wrappedAdapter;
		StylerWrapperListAdapter(ListAdapter adapter) {
			if (adapter == null) {
				if (Config.WTF) Log.wtf(TAG, "StylerWrapperListAdapter got null adapter");
			}
			wrappedAdapter = adapter;
		}

		@Override
		public ListAdapter getWrappedAdapter() {
			return wrappedAdapter;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = wrappedAdapter.getView(position, convertView, parent);

			if (m_items_color != null && position < m_items_color.length) {
				TextView textView = (TextView) view.findViewById(android.R.id.text1);
				textView.setTextColor(m_items_color[position]);
			}

			return view;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return wrappedAdapter.areAllItemsEnabled();
		}

		@Override
		public boolean isEnabled(int position) {
			return wrappedAdapter.isEnabled(position);
		}

		@Override
		public int getCount() {
			return wrappedAdapter.getCount();
		}

		@Override
		public Object getItem(int position) {
			return wrappedAdapter.getItem(position);
		}

		@Override
		public long getItemId(int position) {
			return wrappedAdapter.getItemId(position);
		}

		@Override
		public int getItemViewType(int position) {
			return wrappedAdapter.getItemViewType(position);
		}

		@Override
		public int getViewTypeCount() {
			return wrappedAdapter.getViewTypeCount();
		}

		@Override
		public boolean hasStableIds() {
			return wrappedAdapter.hasStableIds();
		}

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
			wrappedAdapter.registerDataSetObserver(observer);
		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
			wrappedAdapter.unregisterDataSetObserver(observer);
		}

		@Override
		public boolean isEmpty() {
			return wrappedAdapter.isEmpty();
		}
	}

	@Override
	public void setAdapter(SpinnerAdapter adapter) {
		throw new RuntimeException("setAdapter is not supported by MultiStringSpinner.");
	}

	public void setColors(int[] list) {
		m_items_color = list;
	}

	public void setColors(List<Integer> list) {
		if (list == null) {
			m_items_color = null;
		} else {
			m_items_color = new int[list.size()];
			for (int i = 0; i < list.size(); i++) {
				m_items_color[i] = list.get(i);
			}
		}
	}

	public void setItems(String[] items) {
		if (items == null) {
			m_items = null;
			m_selectedItems = null;
			m_simpleStringAdapter = null;
			return;
		}

		m_items = items;
		m_selectedItems = new boolean[m_items.length];
		m_simpleStringAdapter.clear();
		m_simpleStringAdapter.add(m_items[0]);
		Arrays.fill(m_selectedItems, false);
	}

	public void setItems(List<String> items) {
		setItems((items.size() > 0) ? items.toArray(new String[items.size()]) : null);
	}



	public String[] getItems() {
		return m_items;
	}



	public void setSelectionString(String selection) {
		if (m_items != null) {
			setSelection(Arrays.asList(m_items).indexOf(selection));
		}
	}

	public void setSelectionString(String[] selections) {
		if (selections == null) {
			throw new IllegalArgumentException("selections cannot be null");
		}

		if (m_items == null) {
			return;
		}

		ArrayList<Integer> selectedIndices = new ArrayList<Integer>();
		for (String sel : selections) {
			int index = Arrays.asList(m_items).indexOf(sel);
			if (index < 0) {
				continue; /* Report error? */
			}
			selectedIndices.add(index);
		}

		setSelection(selectedIndices);
	}

	public void setSelectionString(List<String> selections) {
		if (selections == null) {
			throw new IllegalArgumentException("selections cannot be null");
		}

		setSelectionString(selections.toArray(new String[selections.size()]));
	}


	public void setSelection(List<Integer> selectedIndicies) {
		if (selectedIndicies == null) {
			throw new IllegalArgumentException("selections cannot be null");
		}

		setSelection(selectedIndicies.toArray(new Integer[selectedIndicies.size()]));
	}

	public void setSelection(Integer[] selectedIndicies) {
		if (selectedIndicies == null) {
			throw new IllegalArgumentException("selections cannot be null");
		}

		if (m_selectedItems == null || m_simpleStringAdapter == null) {
			return;
		}

		for (int i = 0; i < m_selectedItems.length; i++) {
			m_selectedItems[i] = false;
		}

		for (int index : selectedIndicies) {
			if (index >= 0 && index < m_selectedItems.length) {
				m_selectedItems[index] = true;
			} else {
				throw new IllegalArgumentException("Index " + index + " is out of bounds.");
			}
		}

		m_simpleStringAdapter.clear();
		m_simpleStringAdapter.add(concatSelectedStrings());
	}



	public List<String> getSelectedStrings() {
		List<String> selections = new ArrayList<String>();

		if (m_selectedItems == null || m_items == null) {
			for (int i = 0; i < m_items.length; ++i) {
				if (m_selectedItems[i]) {
					selections.add(m_items[i]);
				}
			}
		}

		return selections;
	}

	public List<Integer> getSelectedIndicies() {
		List<Integer> selections = new ArrayList<Integer>();

		if (m_selectedItems != null && m_items != null) {
			for (int i = 0; i < m_items.length; ++i) {
				if (m_selectedItems[i]) {
					selections.add(i);
				}
			}
		}

		return selections;
	}



	private String concatSelectedStrings(String glue) {
		if (m_selectedItems == null || m_items == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		boolean foundOne = false;

		for (int i = 0; i < m_items.length; ++i) {
			if (m_selectedItems[i]) {
				if (foundOne) {
					sb.append(glue);
				}

				foundOne = true;

				sb.append(m_items[i]);
			}
		}

		return sb.toString();
	}

	public String concatSelectedStrings() {
		return concatSelectedStrings(", ");
	}


	/* ref: http://stackoverflow.com/a/3542895/1500988 { */
	@Override
	public Parcelable onSaveInstanceState() {
		/* Theirs nothing for us to save */
		if (m_items == null) {
			return super.onSaveInstanceState();
		}

		/* save state (including parent class) */
		SavedState ss = new SavedState(super.onSaveInstanceState());
		ss.m_items = getItems();

		/* Convert List<Integer> to int[] and pass on to SavedState */
		List<Integer> selectedIndices = getSelectedIndicies();
		ss.m_selectedItems = new int[selectedIndices.size()];
		for (int i = 0; i < selectedIndices.size(); i++) {
			ss.m_selectedItems[i] = selectedIndices.get(i);
		}

		return ss;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof SavedState) {
			SavedState ss = (SavedState) state;
			state = ss.getSuperState();

			setItems(ss.m_items);

			/* Convert List<Integer> to int[] and pass on to SavedState */
			List<Integer> selectedIndices = new ArrayList<Integer>();
			for (int val : ss.m_selectedItems) {
				selectedIndices.add(val);
			}
			setSelection(selectedIndices);
		}

		super.onRestoreInstanceState(state);
	}

	static class SavedState extends BaseSavedState {
		String[] m_items;
		int[] m_selectedItems;

		SavedState(Parcelable superState) {
			super(superState);
		}

		private SavedState(Parcel in) {
			super(in);

			/* Restore */
			this.m_items = new String[in.readInt()];
			in.readStringArray(this.m_items);
			this.m_selectedItems = new int[in.readInt()];
			in.readIntArray(this.m_selectedItems);
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);

			/* Store */
			out.writeInt(this.m_items.length);
			out.writeStringArray(this.m_items);
			out.writeInt(this.m_selectedItems.length);
			out.writeIntArray(this.m_selectedItems);
		}

		/* Parcelable protocol requirement */
		public static final Parcelable.Creator<SavedState> CREATOR =
			new Parcelable.Creator<SavedState>() {
				public SavedState createFromParcel(Parcel in) {
					return new SavedState(in);
				}

				public SavedState[] newArray(int size) {
					return new SavedState[size];
				}
			};
	}

	/* } */
}

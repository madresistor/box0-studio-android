/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0_studio.ui;

import android.widget.ViewSwitcher;
import android.content.Context;
import android.os.Parcelable;
import android.os.Parcel;
import android.util.AttributeSet;

/**
 * ViewSwitcher do not save the current displayed child.
 */
public class CorrectedViewSwitcher extends ViewSwitcher {
	public CorrectedViewSwitcher(Context context) {
		super(context);
	}

	public CorrectedViewSwitcher(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		SavedState s = new SavedState(superState);
		s.displayedChild = getDisplayedChild();
		return s;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof SavedState) {
			SavedState s = (SavedState) state;
			setDisplayedChild(s.displayedChild);
			state = s.getSuperState();
		}

		super.onRestoreInstanceState(state);
	}

	static class SavedState extends BaseSavedState {
		public int displayedChild;

		public SavedState(Parcelable superState) {
			super(superState);
		}

		private SavedState(Parcel in) {
			super(in);
			displayedChild = in.readInt();
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeInt(displayedChild);
		}

		public static final Parcelable.Creator<SavedState> CREATOR =
			new Parcelable.Creator<SavedState>() {
				public SavedState createFromParcel(Parcel in) {
					return new SavedState(in);
				}

				public SavedState[] newArray(int size) {
					return new SavedState[size];
				}
		};
	}
}

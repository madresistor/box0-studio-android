/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (C) 2015 Rishabh Pugalia <rishabh.pugalia@gmail.com>
 * Source: http://codetheory.in/saving-user-settings-with-android-preferences/
 * Licence: Apache License, Version 2.0
 *
 * Copyright (C) 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 * Licence: GPLv3 or later
 * ChangeLog:
 *  - NumberPickerPreference -> LineWidthPreference
 *  - default value (0 - > 3) and max min changed from (0, 100) -> (1,99)
 *  - instead of inflating, instanciating NumberPicker
 *  - changed mValue and mNumberPicker to m_value and m_numberPicker
 */

package com.madresistor.box0_studio.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

import com.madresistor.box0_studio.R;

public class LineWidthPreference extends DialogPreference {
	private static String TAG = LineWidthPreference.class.getName();

	private final int DEFAULT_VALUE = 3;

	Integer m_value;
	NumberPicker m_numberPicker;

	/*
	* We declare the layout resource file as well as the
	* text for the positive and negative dialog buttons.
	* */
	public LineWidthPreference(Context context, AttributeSet attrs) {
		super(context, attrs);

		setPositiveButtonText(R.string.done);
		setNegativeButtonText(R.string.cancel);
	}

	@Override
	protected View onCreateDialogView() {
		Context context = getContext();
		return new NumberPicker(context);
	}

	/*
	* Bind data to our content views
	* */
	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);

		/* Set min and max values to our NumberPicker */
		m_numberPicker = (NumberPicker) view;
		m_numberPicker.setMinValue(1);
		m_numberPicker.setMaxValue(99);

		/* Set default/current/selected value if set */
		if (m_value != null) {
			m_numberPicker.setValue(m_value);
		}
	}

	/*
	* Called when the dialog is closed.
	* If the positive button was clicked then persist
	* the data (save in SharedPreferences by calling `persistInt()`)
	* */
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		if (positiveResult) {
			m_value = m_numberPicker.getValue();
			persistInt(m_value);
		}
	}

	/*
	* Set initial value of the preference. Called when
	* the preference object is added to the screen.
	*
	* If `restorePersistedValue` is true, the Preference
	* value should be restored from the SharedPreferences
	* else the Preference value should be set to defaultValue
	* passed and it should also be persisted (saved).
	*
	* `restorePersistedValue` will generally be false when
	* you've specified `android:defaultValue` that calls
	* `onGetDefaultValue()` (check below) and that in turn
	* returns a value which is passed as the `defaultValue`
	* to `onSetInitialValue()`.
	* */
	@Override
	protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
		if (restorePersistedValue) {
			m_value = getPersistedInt(DEFAULT_VALUE);
		} else {
			m_value = (Integer) defaultValue;
			persistInt(m_value);
		}
	}

	/*
	* Called when you set `android:defaultValue`
	*
	* Just incase the value is undefined, you can return
	* DEFAULT_VALUE so that it gets passed to `onSetInitialValue()`
	* that gets saved in SharedPreferences.
	* */
	@Override
	protected Object onGetDefaultValue(TypedArray a, int index) {
		return a.getInteger(index, DEFAULT_VALUE);
	}
}

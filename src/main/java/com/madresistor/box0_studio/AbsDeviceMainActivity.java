/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014, 2015 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.madresistor.box0_studio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.madresistor.box0.Device;
import com.madresistor.box0_studio.device.DeviceInfoDialog;
import com.madresistor.box0_studio.device.DeviceManager;
import com.madresistor.box0_studio.device.DeviceSearchDialog;

abstract class AbsDeviceMainActivity extends AppCompatActivity
		implements DeviceManager.OnStatusChangedListener{
	private final static String TAG = AbsDeviceMainActivity.class.getName();

	private DeviceManager m_deviceManager;

	@Override
	protected void onCreate(Bundle outBundle) {
		super.onCreate(outBundle);

		/* Why here?
		 * Ref: http://stackoverflow.com/a/29819543/1500988
		 */
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.main_activity_toolbar);
		setSupportActionBar(toolbar);

		m_deviceManager = new DeviceManager(this);
		m_deviceManager.setOnStatusChangedListener(this);
		m_deviceManager.search(true);
	}

	public void onStatusChanged(DeviceManager deviceManager, int msg) {
		switch(msg) {
		case DeviceManager.OnStatusChangedListener.DEVICE_CONNECTED:
			/* goto a device */
			Toast.makeText(this,
				R.string.device_connected,
				Toast.LENGTH_SHORT).show();
			deviceChanged(deviceManager.getDevice());
		break;
		case DeviceManager.OnStatusChangedListener.DEVICE_DISCONNECTED:
			/* remove the device */
			/* long length toast because the user only do is look at the toast */
			Toast.makeText(this,
				R.string.device_disconnected,
				Toast.LENGTH_LONG).show();
			deviceChanged(null);
		break;
		case DeviceManager.OnStatusChangedListener.DEVICE_ERROR:
			/* error while processing device */
			showDeviceSearchDialog(R.string.device_error);
		break;
		case DeviceManager.OnStatusChangedListener.PERMISSION_DENIED:
			/* do nothing, permission denied. user dont want to do this */
		break;
		case DeviceManager.OnStatusChangedListener.DEVICE_INVALID:
			/* invalid device?? */
			showDeviceSearchDialog(R.string.device_error);
		break;
		case DeviceManager.OnStatusChangedListener.DEVICE_NOT_FOUND:
			/* no device found */
			showDeviceSearchDialog(R.string.device_not_found);
		break;
		}
	}

	/* loop in dialog that will prompt user to search for device */
	private void showDeviceSearchDialog(int resIdTitle) {
		DeviceSearchDialog dialog;
		dialog = new DeviceSearchDialog(m_deviceManager, this, resIdTitle);
		dialog.show();
	}

	private MenuItem m_actionSearchDevice, m_actionDeviceInfo;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_activity_actions, menu);
		m_actionSearchDevice = menu.findItem(R.id.main_activity_search_device);
		m_actionDeviceInfo = menu.findItem(R.id.main_activity_device_info);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
		switch(item.getItemId()) {
		case R.id.main_activity_search_device:
			m_deviceManager.search(false);
		return true;
		case R.id.main_activity_device_info:
			Device d = m_deviceManager.getDevice();
			DeviceInfoDialog dialog = new DeviceInfoDialog(this, d);
			dialog.show();
		return true;
		default:
		return super.onOptionsItemSelected(item);
		}
	}

	/*
	 * Ref: http://stackoverflow.com/questions/17828869
	 */
	@Override
	protected void onResume() {
		super.onResume();

		Intent intent = getIntent();
		if (intent != null) {
			distributeIntent(intent);
		}
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		distributeIntent(intent);
	}

	private void distributeIntent(Intent intent) {
		m_deviceManager.consumeIndent(intent);
	}

	/***
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		Device d = m_deviceManager.getDevice();
		m_actionSearchDevice.setVisible(d == null);
		m_actionDeviceInfo.setVisible(d != null);
		super.onPrepareOptionsMenu(menu);
		return true;
	}

	abstract protected void deviceChanged(Device box0Device);
};

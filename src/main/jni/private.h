/*
 * This file is part of box0-studio-android.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * box0-studio-android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * box0-studio-android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with box0-studio-android.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX0_STUDIO_JAVA_PRIVATE_H
#define BOX0_STUDIO_JAVA_PRIVATE_H

#include <jni.h>
#include <android/log.h>
#include "common.h"
#include <jbox0/jbox0.h>

__BEGIN_DECLS

#define UNUSED(var) ((void)var)

#define THROW_RUNTIME_EXCEPTION(env, msg) \
			throw_exception(env, "java/lang/RuntimeException", msg)
void throw_exception(JNIEnv *env, const char *cls, const char *msg);

#define ThrowBox0Exception(r) jbox0_throw_result_exception(env, r)

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved);

extern JavaVM *cachedJVM;

#define LOG_TAG "box0-studio-jni"

#if !defined(NDEBUG)
# define LOG_DEBUG(fmt, ...) __android_log_print(ANDROID_LOG_DEBUG,		\
	LOG_TAG, "%s:%i: " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
# define LOG_DEBUG(msg, ...)
#endif

#define LOG_WARN(fmt, ...) __android_log_print(ANDROID_LOG_WARN,		\
	LOG_TAG, "%s:%i: " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LOG_DEBUG_INT(variable) LOG_DEBUG(#variable ": %i", variable)

#define LOG_GL_ERROR(cmd) {												\
		cmd;															\
		LOG_GL_ERROR_TEST(#cmd);											\
	}

#if !defined(NDEBUG)
# define LOG_GL_ERROR_TEST(msg) {										\
		GLint _err = glGetError();										\
		if (_err != GL_NO_ERROR) {										\
			LOG_WARN("glGetError [%i]: %s", _err, msg);					\
		}																\
	}
#else
# define LOG_GL_ERROR_TEST(msg)
#endif

#define JNI_NULL 0

#define EXTRACT_POINTER(env, obj, type)	\
	((type *)(*env)->GetDirectBufferAddress(env, obj))

#define RETURN_IF_FAIL(stmt, ...) if (!(stmt)) { return __VA_ARGS__; }
#define RETURN_IF_NULL(ptr, ...) RETURN_IF_FAIL(ptr != NULL, ##__VA_ARGS__)

#define MEM_ALLOC_CHECK(env, ptr, ...)									\
	if (ptr == NULL) {													\
		THROW_RUNTIME_EXCEPTION(env, "memory allocation failed!");		\
		return __VA_ARGS__;												\
	}																	\

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define CLAMP(val, min, max) MAX(MIN(val, max), min)

__END_DECLS

#endif

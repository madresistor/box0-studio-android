#/bin/sh
mkdir drawable-ldpi
mkdir drawable-mdpi
mkdir drawable-hdpi
mkdir drawable-xhdpi
mkdir drawable-xxhdpi
mkdir drawable-xxxhdpi


SVG2PNG=$PWD/svg2png.sh

$SVG2PNG -w48 ic_launcher.svg

$SVG2PNG -w24 ic_func_gen_normal.svg
$SVG2PNG -w24 ic_func_gen_activated.svg

$SVG2PNG -w24 ic_oscilloscope_normal.svg
$SVG2PNG -w24 ic_oscilloscope_activated.svg

$SVG2PNG -w24 ic_pwm_normal.svg
$SVG2PNG -w24 ic_pwm_activated.svg

$SVG2PNG -w24 ic_action_pwm_calc_min_error.svg

$SVG2PNG -w24 ic_led_on.svg
$SVG2PNG -w24 ic_led_off.svg

$SVG2PNG -w24 ic_dio_normal.svg
$SVG2PNG -w24 ic_dio_activated.svg

$SVG2PNG -w48 ic_waveform_constant.svg
$SVG2PNG -w48 ic_waveform_noise.svg
$SVG2PNG -w48 ic_waveform_saw_tooth.svg
$SVG2PNG -w48 ic_waveform_sine.svg
$SVG2PNG -w48 ic_waveform_square.svg
$SVG2PNG -w48 ic_waveform_triangular.svg
